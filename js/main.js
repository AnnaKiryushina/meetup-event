new WOW().init();

jQuery(document).ready(function() {
	var fieldValue ='';
    $(".form__item").focusin(function(){
    	fieldValue = $(this).val();
		$(this).val('');
    });
    $(".form__item").focusout(function(){
		$(this).val(fieldValue);
    });

//Измерения для фото-галерии со спикерами
    var speakersWrapWidth = $('.speakers__wrap').width();
    var speakerItemWidth = speakersWrapWidth * 0.306
    $('.speaker__item').css({
    	"width": speakerItemWidth, 
    	"height": speakerItemWidth
    });
    $('.speakers__wrap').css({"height": speakersWrapWidth});

    $(".view__more-btn").click(function(){
		$(this).val('');
    });

//Секция Прошедшие события
    var galleryWidth = $('.no-mark').width();
    var style1 = [0.5, 0.333];
    var style2 = [0.4, 0.6, 0.23];
    var imgNum;
    var k = 0;
    var fullGalleryLength;
    var invisGalleryLength 

    $('.no-mark').each(function () {
        imgNum = $(this).find('.slide-itemmix');
    })

    if(imgNum.length % 2)
    {
        fullGalleryLength = Math.floor(imgNum.length / 4) * (galleryWidth * 0.333 + galleryWidth * 0.23);
    }
    else
    {
        fullGalleryLength = Math.floor(imgNum.length / 4) * (galleryWidth * 0.333 + galleryWidth * 0.23) + galleryWidth * 0.333;
    }

    invisGalleryLength = fullGalleryLength - galleryWidth * 0.333 - galleryWidth * 0.23;

    for(var i = 0; i < imgNum.length; i++){
        if(i > 3){
            $(imgNum[i]).css({"visibility": "hidden"});       
        } 
    }
    
    //Стиль фотографий в галерее

    var lines = imgNum.length;
    $('.slide-itemmix:nth-child(4n+1)').each(function(){
        $(this).css({
            "width": galleryWidth * style1[0],
            "height": galleryWidth * style1[2]
        });
        $(this).find('.slide__content').css({
            "width": galleryWidth * style1[0],
            "height": galleryWidth * style1[2]
        });
    })

    $('.slide-itemmix:nth-child(4n+2)').each(function(){
        $(this).css({
            "width": galleryWidth * style1[0],
            "height": galleryWidth * style1[2]
        });
        $(this).find('.slide__content').css({
            "width": galleryWidth * style1[0],
            "height": galleryWidth * style1[2]
        });
    })

    $('.slide-itemmix:nth-child(4n+3)').each(function(){
        $(this).css({
            "width": galleryWidth * style2[0],
            "height": galleryWidth * style2[2]
        });
        $(this).find('.slide__content').css({
            "width": galleryWidth * style2[0],
            "height": galleryWidth * style2[2]
        });
    })

    $('.slide-itemmix:nth-child(4n+4)').each(function(){
        $(this).css({
            "width": galleryWidth * style2[1],
            "height": galleryWidth * style2[2]
        });
        $(this).find('.slide__content').css({
            "width": galleryWidth * style2[1],
            "height": galleryWidth * style2[2]
        });
    })

    //Обработка нажатия кнопки View More

    var newH = $('#past_events_white_bg').height() + invisGalleryLength;
    var lastH = $('#past_events_white_bg').height();

    $('.view__more-btn').click(function(){
        if($(this).attr('alt') == 1){
            $('#past_events_white_bg').css({"height": newH + "px"});
            $(imgNum).each(function(){
                $(this).css({"visibility": "visible"});
            });
            $(this).text('Hide');
            $(this).css({'padding-right': '60px', 'padding-left': '60px'});
            $(this).attr('alt', '0');
        }
        else{
            $('#past_events_white_bg').css({"height": lastH + "px"});
            for(var i = 0; i < imgNum.length; i++){
                if(i > 3){
                    $(imgNum[i]).css({"visibility": "hidden"});     
                }
            }
            $(this).text('Veiw More');
            $(this).css({'padding-right': '49px', 'padding-left': '49px'});
            $(this).attr('alt', '1');
        }
    });

    //Координаты для стрелки в footer

    var xCoor = $('#footer__container').offset().left + $('#footer__container').width();
    $('.back_to_top-btn').css({
        'left': xCoor
    });
});
