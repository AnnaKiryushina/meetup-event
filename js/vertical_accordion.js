var is_pressed = 0;
var alt;
var newWhiteSpaceHeight;

$(document).ready(function(){
    $('.icon__plus').click(function(){
        $(this).next().stop().slideToggle();
        if($(this).attr('alt') == 1) //если расписание не развернуто, кнопка "Плюс"
        {
            // меняем на кнопку "Минус"
        	$(this).attr('src', 'data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAhCAQAAAD97QrkAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfhDAoSCyPlsLBFAAAB4ElEQVRIx62WzU7UUBTHf613PurwpTNLlZUBHwCCuHNBDIlmlDdw4UZ8Bt+BjaK+hjEkAsaFAU3cIshmyhLHaIKKzsjfBZ3rnaaddqr/pklve8/vnp6ee049kahZbjDHNA1GOeKQXbZ4wTuSpit+BFrWRyVrXw90Nm4RBzTV0mCFupOOqGhV+fRM1STEmDZyAiTplcbjiLLWhwBI0rrK/YjHQwIk6amLWCoAkKSlHiJQWBBxoED4wF0uUkwXuHeaWnsFfZCkfXmeZtl2UhWUsbIXnT3NGxbs4IQux3Q5GQDwMVQx+PbOguGq9aBLm5DP/Er1xKPMeS5Rp2Q9mTNM2QnHhGyyxvcI8Zv30ZNJzkWICte5RY2StZo2NPq82OF1wou0aNnrCa7RRdaLhs+4E4sO3zLD+YNO3yIjPl+dUJWo9UU7SQElJ5hw5HNoQ2Woc4UzGYhJ6hhnoU+enrMYxaJDm5ADfiJGcn8RDG8ixKkXNS7nyou/Xjz0NMNbOyyUnQh9+Lc94gMrFNdKr+xnVe00hQp6VatZEHHbrZ2PCgCe/PcKjkb1cgjAhsaSulk5dzdbVSW9p97M/DotNQe3ZVTVfe2kmO9pWUHcwkv9v1hkhinqTPCFNrtsseZsBUd/ALW8Rrcw1OBTAAAAAElFTkSuQmCC');
        	$(this).attr('alt', '0');
            //alert($('#white_rotated_space').height());
            newWhiteSpaceHeight = $('#white_rotated_space').height() + 120;
            $('#white_rotated_space').css({"height": newWhiteSpaceHeight + "px"});
        }
        else //если расписание развернуто, кнопка "Минус"
        {	
            // меняем на кнопку "Плюс"	
        	$(this).attr('src', 'data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAhCAQAAAD97QrkAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfhDAoNMRxhBadMAAABpUlEQVRIx62Wy0pbURSGv2xCNGKMVQdOhKpt9QWU+AIighL1MVR8FyfV6rTQYTsqag2CoEEnzjReQMWZFqIGL0j5ncRjstk7t9O1Jou11/7OvpzzrxMRThtmjBSDdJGgwDU5svxmH1e5bI9rTidy26nm1WLPsAFpXaiyXWraj2jSsmqzVTW7EG3K1AiQpC0lbURMm3UAJGlTsXLEUp0ASVopRcw0AJCkGSEigjg5ehz3/Y+DYtRLh/PtueILjwjNep6RD476u3cdC8IACzRu80QMw3wOgegnZRgNAQAYNYyERKSiDFipn7wUo4cgl6EQxJN0l9UPorx1xklR0bet+ntDMuRGWg23IREFw3VIxE2UYz6Vpc4DcbvjYzH6UXL1CQtxFGWH8bJUexCZ9/3ywbuKXcN6yI1sGPbJVS178o6ckTXAYlXEs3dk8U323ap9p76i//J86peKv6lWukHVmirVzq8NAL79dwVHCf2pA5BRm6ubxWruZstq8vfUiao99ULpym0ZNWtWh57px5pT3J4R8f5fjDPEAJ20k+cvObKssecqfQW/g9JgAjzQOgAAAABJRU5ErkJggg==');
        	$(this).attr('alt', '1');
            newWhiteSpaceHeight -= 120;
            $('#white_rotated_space').css({"height": newWhiteSpaceHeight + "px"});
        }		

    }).next().stop().hide();
 
});